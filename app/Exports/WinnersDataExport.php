<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class WinnersDataExport implements FromCollection, WithHeadings
{
    use Exportable;

    private $export;

    public function __construct($export)
    {
        $this->export = $export;
    }

    public function collection()
    {
        return collect($this->export);
    }

    public function headings(): array
    {
        return [
            'Sorszám',
            'Név',
            'Telefonszám',
        ];
    }

}
