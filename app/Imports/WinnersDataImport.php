<?php

namespace App\Imports;

use App\Models\Participant;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class WinnersDataImport implements ToModel, WithHeadingRow, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Participant([
            'first_name' => $row['first_name'],
            'last_name'=>$row['last_name'],
            'phone'=>$row['phone'],
        ]);
    }

    public function rules(): array
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            //'phone' => 'required|regex:/(01)[0-9]{9}/'
            'phone' => 'required'
        ];
    }
}
