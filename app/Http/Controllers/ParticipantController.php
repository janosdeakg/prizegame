<?php

namespace App\Http\Controllers;

use App\Imports\WinnersDataImport;
use App\Models\Participant;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;

class ParticipantController extends Controller
{
    // Show Creat Form
    public function create ()
    {
        return view('participant.create', [
            'title' => 'Résztvevő hozzáadása',
            'page' => 'page_1',
            'auth' => true,
        ]);
    }

    // Store participant data
    public function store(Request $request) {
        $formFields = $request->validate([
            'last_name' => 'required',
            'first_name' => 'required',
            'phone' => ['required',  Rule::unique('participant', 'phone')]
        ]);

        Participant::create($formFields);

        return redirect('/add-participant')->with('success', 'Résztvevő hozzáadva!');
    }

    // Store participant data from excel
    public function import(Request $request)
    {
        if($request->hasFile('participantExcel')) { 
            $file = $request->file('participantExcel');
            $extension = $file->getClientOriginalExtension();

            if ($extension == 'xlsx') {
                
                try {
                    Excel::import(new WinnersDataImport, $file);
                    return redirect('/add-participant')->with('success', 'Participants Imported Successfully');
                } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
                     $failures = $e->failures();
                     
                     foreach ($failures as $failure) {
                         $failure->row(); // row that went wrong
                         $failure->attribute(); // either heading key (if using heading row concern) or column index
                         $failure->errors(); // Actual error messages from Laravel validator
                         $failure->values(); // The values of the row that has failed.
                     }

                     return redirect('/add-participant')->with('warning', $e->getMessage());
                }
                
            } else {
                return redirect('/add-participant')->with('warning', 'The selected file must be a Microsoft Excel file(xlsx)!');
            }
        } else {
            return redirect('/add-participant')->with('warning', 'Please select a file to upload!');
        }
    }
}
