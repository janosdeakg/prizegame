<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    //Show login form
    public function login ()
    {
        return view('users.login', [
            'title' => 'Bejelentkezés',
            'page' => 'page_1',
            'auth' => false,
        ]);
    }

    // Log in user
    public function authenticate ()
    {
        return redirect('/add-participant');
    }

}
