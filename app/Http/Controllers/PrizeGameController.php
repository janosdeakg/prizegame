<?php

namespace App\Http\Controllers;

use App\Models\Participant;

class PrizeGameController extends Controller
{
    public function index()
    {
        return view('prizegame.index', [
            'title' => 'Prizegame',
            'participantCount' => Participant::count(),
            'page' => 'page_2',
            'auth' => true,
        ]);
    }
}
