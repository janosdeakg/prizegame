<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Participant;
use App\Exports\WinnersDataExport;
use Maatwebsite\Excel\Facades\Excel;

class Winners extends Component
{
    public $participants = [];
    public $participantCount;
    public $winner = 1;

    public function mount ()
    {
        $this->participantCount = Participant::count();
    }
    
    public function add($winner)
    {
        
        $participant = Participant::orderBy('id')->get();
        $participant = $participant[$winner-1];

        if ($participant) {

            foreach ($this->participants as $exParticipant) {
                if ($exParticipant['id'] == $participant['id']) return false;
            }

            $participant['order'] = $this->winner;
            array_push($this->participants, $participant);
        }
        $this->winner++;
    }

    public function export ()
    {
        foreach ($this->participants as $participant) {
            $export[] = [
                'order' => $participant['order'],
                'name' => $participant['last_name'].' '.$participant['first_name'],
                'phone' => $participant['phone'],
            ];
        }
        
        return Excel::download(new WinnersDataExport($export), 'nyertesek.xlsx');
    }
    
    public function render()
    {
        return view('livewire.winners');
    }
}
