/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
  ],
  theme: {
    extend: {
      colors: {
        cgpyellow: "#ffab01",
        cgpgreen: "#00575f",
        cgpgreen_hover: "#02909e",
        cgppurple: "#6610f1",
        cgpcyan: "#59c6c6"
      },
      fontFamily: {
        'CALIBRIB' : ['CALIBRIB'],
        'CALIBRIL' : ['CALIBRIL'],
        'LoRes' : ['LoRes'],
      }
    },
  },
  plugins: [],
}
