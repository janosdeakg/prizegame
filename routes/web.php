<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PrizeGameController;
use App\Http\Controllers\ParticipantController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


//Route::get('click-event', Lottery::class);

// Redirect to login as default
Route::get('/', function () {
    return redirect('/login');
});

// Show prizegame
Route::get('/prizegame', [PrizeGameController::class, 'index']);

// Show participant create form
Route::get('/add-participant', [ParticipantController::class, 'create']);

// Import participant data from excel
Route::post('/participant/import', [ParticipantController::class, 'import']);

// Store participant data
Route::post('/participant', [ParticipantController::class, 'store']);

// Show login form
Route::get('/login', [UserController::class, 'login']);

// Log in user
Route::post('/users/authenticate', [UserController::class, 'authenticate']);

