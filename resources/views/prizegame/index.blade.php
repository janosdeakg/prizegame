@extends('layout')

@section('partials')
    @include('partials._count')
    @include('partials._description')
@endsection

@section('content')

<div>
    <div class="px-[5%] ">
       
    </div>

    <div>
        <livewire:winners/>
    </div>

    @livewireScripts
</div>
@endsection