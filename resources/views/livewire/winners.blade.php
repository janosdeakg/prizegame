<div>
    <!-- LOTTERY -->

    <section class="bg-cgppurple relative bg-laravel flex flex-col justify-center align-center text-center pt-20">

        <!-- Script for random number generation -->
        <script>
            function appLotteryComponent(maxNum = {{$participantCount}}, seconds = 30) {
                return {
                    
                    current: 0,
                    startLottery() {

                        this.myInterval = setInterval(() => {   
                            this.current = Math.floor(Math.random() * (maxNum - 1 + 1)) + 1;
                        
                            }, seconds);
                        
                    },
                    getNumber() {
                        return this.current;
                    },
                    stopLottery() {
                        clearInterval(this.myInterval);
                    }
                }
            } 
        </script>

        <div class="">
            @unless($participantCount < 2)
                <!-- Use appLoterryComponent as source -->
                <div x-data="appLotteryComponent()">

                    <!-- Start  random number generation -->
                    <button x-on:click="startLottery()" class="bg-cgpcyan hover:bg-cgpgreen_hover text-white py-2 px-8 rounded-full">
                        SORSOLÁS START
                    </button>

                    <!-- Stop random number generation (clearInterval) and use mgaic wire to call the livewire add function -->
                    <button x-on:click="stopLottery(); $wire.add(current)" class="bg-cgpcyan hover:bg-cgpgreen_hover text-white py-2 px-8 rounded-full">
                        SORSOLÁS STOP
                    </button>

                    <!-- Display the generated random number -->
                    <h1 x-text="current" class="font-LoRes text-[200px] font-bold uppercase text-white"></h1>
                </div>
            @else
                <h1 class="text-lg font-bold uppercase text-white pb-20">
                    Kérem adjon résztvevőket a játékhoz!
                </h1>
            @endunless
        </div>
        

    </section>

    <!-- WINNERS -->

    <!-- Display winner cards -->
    <div class="lg:grid lg:grid-cols-3 gap-10  md:space-y-0 bg-violet-200 py-14 px-48">
        @unless(count($participants) == 0)
        @foreach($participants as $participant)
            <x-winner-card :participant="$participant" />
        @endforeach
        @endunless
    </div>

    <!-- Export selected winners to excel -->
    <div class="bg-violet-200 justify-center align-center text-center">
        <button wire:click="export" class="bg-transparent border-2 border-slate-900 hover:bg-cgpgreen_hover text-black py-2 px-8 rounded-full mb-16 mt-16"
        @if(count($participants) == 0) disabled  @endif
        >
            Nyertesek adatainak letöltése
        </button>
    </div>

</div>