@extends('layout')

@section('form')

<div class="">
    <div class="z-50 w-[35%] float-right box-shadow.lg">
        <div class="bg-gray-50 border border-gray-200 bg-opacity-75 text-center">
            <div class="mt-24 px-[10%]">
                <form method="POST" action="/users/authenticate" class="w-full">
                    @csrf

                    <div class="mb-6">
                        <input
                            type="text"
                            class="border border-slate-500  p-2 w-full"
                            name="username" value="{{old('username')}}"
                            placeholder="Username"
                        />
                        @error('username')
                            <p class="text-red-500 text-xs mt-1">{{$message}}</p>
                        @enderror
                    </div>

                    <div class="mb-6">
                        <input
                            type="password"
                            class="border border-slate-500  p-2 w-full"
                            name="password" value="{{old('password')}}"
                            placeholder="Password"
                        />
                        @error('password')
                            <p class="text-red-500 text-xs mt-1">{{$message}}</p>
                        @enderror
                    </div>

                    <div class="mb-24 mt-20">
                        <button
                            type="submit"
                            class="bg-cgpgreen text-xs text-white rounded py-4 px-14 hover:bg-black rounded-full px-16 uppercase"
                        >
                            Login
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="flex flex-row mt-6 justify-between items-center">
            <img class="h-16" src="{{asset('images/logos/green_logo.svg')}}" alt="" class="logo"/>
            <span class="text-xs text-white text-base ml-4">WE HAVE BROUGHT EAP AND THE PROGRAM COMMUNICATION TO YOU WITH AN ENVIROMENT FRIENDLY APPROACH</span>
        </div>
    </div>
</div>

@endsection