@extends('layout')

@section('form')

<div class="pt-[3%] px-[5%] mt-[0%] w-full">
    <div class="z-50 w-[35%] m-auto box-shadow.lg">
<div
    class="bg-gray-50 border border-gray-200 bg-opacity-75 p-10" 
>
    <header class="text-center">
        <h2 class="text-2xl font-bold uppercase mb-1">
            Résztvevő hozzáadás
        </h2>
        <p class="mb-4">Adjon hozzá egy résztvevőt a sorsoláshoz</p>
    </header>

    <form method="POST" action="/participant">
        @csrf
        <div class="mb-6">
            <input
                type="text"
                class="border border-slate-500 p-2 w-full"
                name="last_name"
                placeholder="Vezetéknév"
                value="{{old('last_name')}}"
                required
            />
            @error('last_name')
                <p class="text-red-500 text-xs mt-1">{{$message}}</p>
            @enderror
        </div>

        <div class="mb-6">
            <input
                type="text"
                class="border border-slate-500 p-2 w-full"
                name="first_name"
                placeholder="Keresztnév"
                value="{{old('first_name')}}"
                required
            />
            @error('first_name')
                <p class="text-red-500 text-xs mt-1">{{$message}}</p>
            @enderror
        </div>

        <div class="mb-6">
            <input
                type="text"
                class="border border-slate-500 rounded p-2 w-full"
                name="phone"
                placeholder="Telefonszám"
                value="{{old('phone')}}"
                required
            />
            @error('phone')
                <p class="text-red-500 text-xs mt-1">{{$message}}</p>
            @enderror
        </div>

        <div class="mb-6">
            <button
                class="bg-cgpgreen text-xs text-white rounded py-4 px-14 hover:bg-black rounded-full px-16 uppercase"
            >
                Hozzáad
            </button>

            <a href="/" class="text-xs text-black ml-4 hover:text-black uppercase"> Vissza </a>
        </div>

    </form>
    <div>
        <form action="/participant/import" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="file" name="participantExcel" id="participantExcel">
            <button onclick="form.submit()">UPLOAD</button>
        </form>
    </div>
</div>
</div>
</div>

<!-- SweetAlert2 popup -->
@if (session('success'))
    <script type="module">
      Swal.fire("Siker!", "{{ Session::get('success') }}", 'success', {
            button:true,
            button:"OK"
        });
    </script>
@endif

@if (session('warning'))
    <script type="module">
      Swal.fire("Hiba!", "{{ Session::get('warning') }}", 'warning', {
            button:true,
            button:"OK"
        });
    </script>
@endif

@endsection