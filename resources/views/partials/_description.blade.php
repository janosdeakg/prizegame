<section class="relative h-60 bg-cgppurple opacity-75 flex flex-col justify-center align-center text-left">

    <div class="px-32">
        <p  class="font-CALIBRIL text-white">
            Nam in tempor ligula. Nunc nunc metus, egestas nec magna quis, luctus posuere massa. 
            Morbi venenatis odio sit amet vulputate venenatis. Nullam tincidunt felis iaculis bibendum interdum. 
            Nullam sollicitudin magna tellus, id feugiat risus faucibus vel. Pellentesque quis nisi eget nunc vehicula vestibulum. 
            Fusce condimentum elit at tellus vehicula, quis elementum velit varius. Mauris sit amet scelerisque nisl. 
            Suspendisse viverra dolor a felis tempus convallis. Nunc lacinia nunc in placerat auctor.
            Nunc non libero et est pulvinar efficitur eget non turpis. Suspendisse eu dui nec dui egestas aliquet non ac risus.
        </p>
    </div>
</section>