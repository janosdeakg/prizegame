
<section class="mt-4 relative h-28 bg-cgpcyan opacity-75 flex flex-col justify-center align-center text-center">

<div class="z-10">
    <h1 class="text-3xl font-bold uppercase text-black">
        A nereményjátékban résztvevő játékosok száma: {{$participantCount}} fő
    </h1>
    <h2>
        @if ($participantCount <= 1)
            <br>Kérem adjon résztvevőket a játékhoz!
        @endif
    </h2>
</div>
</section>