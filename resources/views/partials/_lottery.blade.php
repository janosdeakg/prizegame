
<section
class="bg-cgppurple relative h-72 bg-laravel flex flex-col justify-center align-center text-center space-y-4"
>
<div
    class="absolute top-0 left-0 w-full h-full opacity-10 bg-no-repeat bg-center"
    style="background-image: url('images/laravel-logo.png')"
></div>


<script>
    function appLotteryComponent(maxNum = {{$participantCount}}, seconds = 30) {
        return {
            
            current: 0,
            startLottery() {

              this.myInterval = setInterval(() => {   
                this.current = Math.floor(Math.random() * (maxNum - 1 + 1)) + 1;
            
                }, seconds);
                
            },
            getNumber() {
                return this.current;
            },
            stopLottery() {
                clearInterval(this.myInterval);
            }
        }
    } 
</script>

<div class="z-10">
    <div x-data="appLotteryComponent()">

        <button x-on:click="startLottery()" class="bg-cgpcyan hover:bg-cgpgreen_hover text-white py-2 px-8 rounded-full">
            SORSOLÁS START
        </button>

        <button wire:click="add()" x-on:click="stopLottery()" class="bg-cgpcyan hover:bg-cgpgreen_hover text-white py-2 px-8 rounded-full">
            SORSOLÁS STOP
        </button>
        
        <h1 x-text="current" style="font-family: LoRes;" class="text-9xl font-bold uppercase text-white mt-6"></h1>
    </div>

</div>

</section>