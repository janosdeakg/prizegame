<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title}}</title>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>
<body>
    
    <div class="min-h-screen bg-cover bg-center bg-no-repeat" style="background-image: url( {{asset('images/backgrounds/'.$page.'.jpeg')}} );">
    <div class="pt-[3%] px-[8%] ">
        <div class="bg-cgpyellow pt-8 pl-8 pr-8 -mb-4">
            <nav class="flex justify-between items-center">
                <div class="flex flex-row">
                    <a href="/">
                        <img class="h-28" src="{{asset('images/logos/header_logo.svg')}}" alt="" class="logo"/>
                    </a>
                    <span class="font-bold text-cgpgreen text-lg ml-2">EAP DASHBOARD</span>
                </div>
                @if($auth)
                <div class="mb-10">
                    <a href="/add-participant" class="text-cgpgreen mr-5 hover:text-black"> Résztvevő hozzáadása</a>
                    <a href="/login" class="bg-cgpgreen hover:bg-black text-white py-2 px-14 rounded-full">
                        Kilépés
                    </a>
                </div>
                @endif
            </nav>
        </div>
        @yield('partials')
        @yield('form');
    </div>
    </div>

    <main>
        @yield('content')
    </main>
    
</body>
</html>