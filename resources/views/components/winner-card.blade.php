@props(['participant'])

<div class="bg-gray-50 border rounded-lg py-8 shadow-xl">       
    <div class="flex flex-col items-center justify-center">
        <img class="h-14 mb-6" src="{{asset('images/prizegame_winner_badge.svg')}}" alt="" />
        <div class="text-center">
            <h3 class="text-4xl uppercase font-bold mb-4">
                Nyertes #{{$participant['order']}}
            </h3>
            <div class="text-xl mb-2">
                <label class="font-bold">Vezetéknév:</label> {{$participant['last_name']}}
            </div>
            <div class="text-xl mb-2">
                <label class="font-bold">Keresztnév:</label> {{$participant['first_name']}}
            </div>
            <div class="text-xl mb-2">
                <label class="font-bold">Telefonszám:</label> {{$participant['phone']}}
            </div>
        </div>
    </div>
</div>