<div {{$attributes->merge(['class' => 'min-h-screen bg-cover bg-center bg-no-repeat py-16 px-72'])}}
    style="background-image: url( {{asset('images/backgrounds/page_2.jpeg')}} );">
    {{$slot}}
</div>