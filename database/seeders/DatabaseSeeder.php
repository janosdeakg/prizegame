<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Participant;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Participant::create(
            [
                'first_name' => 'Gergő',
                'last_name' => 'Jánosdeák',
                'phone' => '06 30 675 3321'
            ]
        );

        Participant::create(
            [
                'first_name' => 'Tamás',
                'last_name' => 'Kovács',
                'phone' => '06 200 355 6556'
            ]
        );

        Participant::create(
            [
                'first_name' => 'Anna',
                'last_name' => 'Kis',
                'phone' => '06 70 231 2566'
            ]
        );
    }
}
